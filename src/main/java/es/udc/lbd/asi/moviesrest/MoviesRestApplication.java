package es.udc.lbd.asi.moviesrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviesRestApplication {

  public static void main(String[] args) {
    SpringApplication.run(MoviesRestApplication.class, args);
  }
}

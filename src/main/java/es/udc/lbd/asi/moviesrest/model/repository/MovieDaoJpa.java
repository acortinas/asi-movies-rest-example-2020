package es.udc.lbd.asi.moviesrest.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.udc.lbd.asi.moviesrest.model.domain.Movie;
import es.udc.lbd.asi.moviesrest.model.repository.util.GenericDaoJpa;

@Repository
public class MovieDaoJpa extends GenericDaoJpa implements MovieDao {

  @Override
  public List<Movie> findAll() {
    return entityManager.createQuery("from Movie", Movie.class).getResultList();
  }

  @Override
  public void create(Movie dbMovie) {
    entityManager.persist(dbMovie);
  }

  @Override
  public Movie findById(Long id) {
    return entityManager.find(Movie.class, id);
  }

  @Override
  public void update(Movie dbMovie) {
    entityManager.merge(dbMovie);
  }

  @Override
  public void delete(Long id) {
    Movie movie = findById(id);
    if (movie != null) delete(movie);
  }

  private void delete(Movie movie) {
    entityManager.remove(movie);
  }
}

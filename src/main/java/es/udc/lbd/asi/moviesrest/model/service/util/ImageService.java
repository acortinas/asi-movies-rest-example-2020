package es.udc.lbd.asi.moviesrest.model.service.util;

import java.io.FileNotFoundException;

import org.springframework.web.multipart.MultipartFile;

import es.udc.lbd.asi.moviesrest.model.exception.AppException;
import es.udc.lbd.asi.moviesrest.model.service.dto.ImageDTO;

public interface ImageService {

  String saveImage(MultipartFile file, Long id) throws AppException;

  ImageDTO getImage(String imagePath, Long id) throws AppException;
}

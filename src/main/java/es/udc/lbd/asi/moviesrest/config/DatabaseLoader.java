package es.udc.lbd.asi.moviesrest.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import es.udc.lbd.asi.moviesrest.model.exception.UserLoginExistsException;
import es.udc.lbd.asi.moviesrest.model.service.MovieService;
import es.udc.lbd.asi.moviesrest.model.service.UserService;
import es.udc.lbd.asi.moviesrest.model.service.dto.MovieDTO;

@Configuration
public class DatabaseLoader {
  private final Logger logger = LoggerFactory.getLogger(DatabaseLoader.class);

  @Autowired private UserService userService;
  @Autowired private MovieService movieService;

  @PostConstruct
  public void init() {
    try {
      if (userService.findAll().size() == 0) {
        userService.registerUser("admin", "admin", true);
        userService.registerUser("user", "user");

        movieService.create(new MovieDTO("Jackie Brown", 1997));
        movieService.create(new MovieDTO("Pulp Fiction", 1994));
        movieService.create(new MovieDTO("Four Rooms", 1995));
        movieService.create(new MovieDTO("Malditos Bastardos", 2009));
        movieService.create(new MovieDTO("Reservoir Dogs", 1992));
      }
    } catch (UserLoginExistsException e) {
      logger.error(e.getMessage(), e);
    }
  }
}

package es.udc.lbd.asi.moviesrest.model.exception;

import es.udc.lbd.asi.moviesrest.model.domain.Movie;

@SuppressWarnings("serial")
public class InstanceNotFoundException extends Exception {

  public InstanceNotFoundException(Long id, Class<Movie> class1) {
    super("No se ha encontrado un elemento " + class1.getName() + " con el id " + id);
  }
}

package es.udc.lbd.asi.moviesrest.model.exception;

@SuppressWarnings("serial")
public class AppException extends Exception {

  public AppException(String msg) {
    super(msg);
  }
}

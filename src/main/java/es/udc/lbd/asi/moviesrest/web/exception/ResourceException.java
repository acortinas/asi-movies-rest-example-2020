package es.udc.lbd.asi.moviesrest.web.exception;

@SuppressWarnings("serial")
public class ResourceException extends Exception {
  public ResourceException(String errorMsg) {
    super(errorMsg);
  }
}

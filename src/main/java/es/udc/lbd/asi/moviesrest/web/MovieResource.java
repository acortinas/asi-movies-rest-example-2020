package es.udc.lbd.asi.moviesrest.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.udc.lbd.asi.moviesrest.model.exception.AppException;
import es.udc.lbd.asi.moviesrest.model.exception.InstanceNotFoundException;
import es.udc.lbd.asi.moviesrest.model.service.MovieService;
import es.udc.lbd.asi.moviesrest.model.service.dto.ImageDTO;
import es.udc.lbd.asi.moviesrest.model.service.dto.MovieDTO;
import es.udc.lbd.asi.moviesrest.web.exception.IdPathNotMathingIdInEntityException;

@RestController
@RequestMapping("/api/movies")
public class MovieResource {

  @Autowired private MovieService movieService;

  @GetMapping
  public List<MovieDTO> findAll() {
    return movieService.findAll();
  }

  @GetMapping("/{id}")
  public MovieDTO findById(@PathVariable Long id) throws InstanceNotFoundException {
    return movieService.findById(id);
  }

  @GetMapping("/{id}/image")
  @ResponseStatus(HttpStatus.OK)
  public void getMovieImageById(@PathVariable Long id, HttpServletResponse response)
      throws InstanceNotFoundException, AppException {
    ImageDTO image = movieService.getMovieImageById(id);

    try {
      response.setContentType(image.getMediaType());
      response.setHeader("Content-disposition", "filename=" + image.getFilename());
      IOUtils.copy(image.getInputStream(), response.getOutputStream());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @PostMapping("/{id}/image")
  @ResponseStatus(HttpStatus.OK)
  public void saveMovieImageById(
      @PathVariable Long id, @RequestParam MultipartFile file, HttpServletResponse response)
      throws InstanceNotFoundException, AppException {

    movieService.saveMovieImageById(id, file);
  }

  @PostMapping
  public MovieDTO create(@RequestBody MovieDTO movie) {
    return movieService.create(movie);
  }

  @PutMapping("/{id}")
  public MovieDTO update(@PathVariable Long id, @RequestBody MovieDTO movie)
      throws IdPathNotMathingIdInEntityException {

    if (id != movie.getId()) {
      throw new IdPathNotMathingIdInEntityException();
    }
    return movieService.update(movie);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable Long id) {
    movieService.delete(id);
  }
}

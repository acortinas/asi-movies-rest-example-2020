package es.udc.lbd.asi.moviesrest.model.repository;

import java.util.List;

import es.udc.lbd.asi.moviesrest.model.domain.Movie;

public interface MovieDao {

  List<Movie> findAll();

  void create(Movie dbMovie);

  Movie findById(Long id);

  void update(Movie dbMovie);

  void delete(Long id);
}

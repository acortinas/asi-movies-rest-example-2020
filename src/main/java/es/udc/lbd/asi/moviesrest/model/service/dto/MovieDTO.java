package es.udc.lbd.asi.moviesrest.model.service.dto;

import es.udc.lbd.asi.moviesrest.model.domain.Movie;

public class MovieDTO {
  private Long id;

  private String title;

  private Integer year;

  private Boolean hasImage = false;

  public MovieDTO() {}

  public MovieDTO(Movie m) {
    this.id = m.getId();
    this.title = m.getTitle();
    this.year = m.getYear();
    if (m.getImagePath() != null) this.hasImage = true;
  }

  public MovieDTO(String title, int year) {
    this.title = title;
    this.year = year;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Boolean getHasImage() {
    return hasImage;
  }
}

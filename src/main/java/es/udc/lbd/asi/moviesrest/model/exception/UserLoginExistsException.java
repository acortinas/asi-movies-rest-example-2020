package es.udc.lbd.asi.moviesrest.model.exception;

@SuppressWarnings("serial")
public class UserLoginExistsException extends Exception {
  public UserLoginExistsException(String msg) {
    super(msg);
  }
}

package es.udc.lbd.asi.moviesrest.model.domain;

public enum UserAuthority {
  USER,
  ADMIN
}

package es.udc.lbd.asi.moviesrest.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Movie {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "movie_generator")
  @SequenceGenerator(name = "movie_generator", sequenceName = "movie_seq")
  private Long id;

  private String title;

  private Integer year;

  private String imagePath;

  public Movie() {}

  public Movie(String title, Integer year) {
    this.title = title;
    this.year = year;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
}

package es.udc.lbd.asi.moviesrest.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import es.udc.lbd.asi.moviesrest.model.domain.Movie;
import es.udc.lbd.asi.moviesrest.model.exception.AppException;
import es.udc.lbd.asi.moviesrest.model.exception.InstanceNotFoundException;
import es.udc.lbd.asi.moviesrest.model.repository.MovieDao;
import es.udc.lbd.asi.moviesrest.model.service.dto.ImageDTO;
import es.udc.lbd.asi.moviesrest.model.service.dto.MovieDTO;
import es.udc.lbd.asi.moviesrest.model.service.util.ImageService;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class MovieService {

  @Autowired private MovieDao movieDao;
  @Autowired private ImageService imageService;

  public List<MovieDTO> findAll() {
    List<Movie> movies = movieDao.findAll();
    List<MovieDTO> moviesDTO =
        movies.stream().map(m -> new MovieDTO(m)).collect(Collectors.toList());
    return moviesDTO;
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public MovieDTO create(MovieDTO movie) {
    Movie dbMovie = new Movie(movie.getTitle(), movie.getYear());
    movieDao.create(dbMovie);
    return new MovieDTO(dbMovie);
  }

  public MovieDTO findById(Long id) throws InstanceNotFoundException {
    Movie movie = movieDao.findById(id);
    if (movie == null) throw new InstanceNotFoundException(id, Movie.class);
    return new MovieDTO(movie);
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public MovieDTO update(MovieDTO movie) {
    Movie dbMovie = movieDao.findById(movie.getId());
    dbMovie.setTitle(movie.getTitle());
    dbMovie.setYear(movie.getYear());
    movieDao.update(dbMovie);
    return new MovieDTO(dbMovie);
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public void delete(Long id) {
    movieDao.delete(id);
  }

  @Transactional(readOnly = false, rollbackFor = Exception.class)
  public void saveMovieImageById(Long id, MultipartFile file)
      throws InstanceNotFoundException, AppException {

    Movie movie = movieDao.findById(id);
    if (movie == null) throw new InstanceNotFoundException(id, Movie.class);

    String filePath = imageService.saveImage(file, movie.getId());
    movie.setImagePath(filePath);
    movieDao.update(movie);
  }

  public ImageDTO getMovieImageById(Long id) throws InstanceNotFoundException, AppException {
    Movie movie = movieDao.findById(id);
    if (movie == null) throw new InstanceNotFoundException(id, Movie.class);

    return imageService.getImage(movie.getImagePath(), movie.getId());
  }
}

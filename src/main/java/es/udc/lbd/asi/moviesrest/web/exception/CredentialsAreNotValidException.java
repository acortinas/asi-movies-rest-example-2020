package es.udc.lbd.asi.moviesrest.web.exception;

@SuppressWarnings("serial")
public class CredentialsAreNotValidException extends ResourceException {

  public CredentialsAreNotValidException(String errorMsg) {
    super(errorMsg);
  }
}
